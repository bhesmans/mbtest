#!/bin/bash

# 1 redsocks port
# 2 ssh server port

echo "base{log_debug=on; log_info=on; log=\"file:/tmp/reddi.log\";daemon = on; redirector = iptables;}
redsocks { local_ip = 0.0.0.0; local_port = $1; ip = 127.0.0.1; port = $2; type = socks5; }" > /root/redsocks.conf

redsocks -c /root/redsocks.conf
