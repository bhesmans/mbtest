#!/bin/bash

# 1 ssh server add
# 2 local port for redsocks
# 3 interface from which we need to redirect

iptables -t nat -N REDSOCKS
iptables -t nat -A REDSOCKS -d 127.0.0.0/8 -j RETURN
iptables -t nat -A REDSOCKS -p tcp -j REDIRECT --to-ports $2
iptables -t nat -A OUTPUT -p tcp -j REDSOCKS
iptables -t nat -I REDSOCKS -d $1 -j RETURN
iptables -t nat -A PREROUTING --in-interface $3 -p tcp -j REDSOCKS 
