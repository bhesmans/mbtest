#!/usr/bin/expect

# 1 : ssh server address
# 2 : ssh server port

set arg1 [lindex $argv 0]
set arg2 [lindex $argv 1]

spawn ssh -fqND $arg2 -o StrictHostKeyChecking=no root@$arg1
expect "root@$arg1's password: "
send "root\r"
expect eof
